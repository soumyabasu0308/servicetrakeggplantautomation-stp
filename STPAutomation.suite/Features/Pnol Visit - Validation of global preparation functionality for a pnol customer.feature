﻿@STP
Feature: Add global preparations
  
  @Sanity
  Scenario Outline: User should be successful to add global preparations for L3 pnol customer visit
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I add new treatment for global preparations with details as <ServiceCover1> <Preparation> <LocationDetails> <Quantity> <Batch> <EPANumber> <ApplicationMethod> <EquipmentUsed> <AreaTreated> <TargetPests> <TreatmentNotes> <Barcode>      
    Then I close the mobile app <SUTDevice> 
    Then I open PNOL Application
    Then I search the site with site reference as <SiteReference> and <CustomerName>
    Then I validate the service point in PNOL with details as <Building> <Floor> <Department><ServicePointDescription>
    Then I navigate to PNOL homepage and close the application
    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails | VisitType | VisitDate | SelectVisitType | ServiceCover1 | Preparation | LocationDetails | Quantity | Batch | EPANumber | ApplicationMethod | EquipmentUsed | AreaTreated | TargetPests | Treatment Notes | Barcode | SiteReference | Building | Floor | Department | ServicePointDescription |
      
      | com.kony.pestservicet |  SUT | Work View | 00820461 | Interserve | Customer | Pests - 12 | R | 21/12/2020 | Routine | Pests - 12 - R | Aquapy | Internal | 10 | BAT10 | 1232QW | Bait Station Updated | Cobra Light Trap | Area10 | American Cockroaches | Test treatment notes to add treatment | 2330858 | UK0082046100002| Global Pesticides Records | All Areas | Internal and External | Pesticides Used |
      
      
      
