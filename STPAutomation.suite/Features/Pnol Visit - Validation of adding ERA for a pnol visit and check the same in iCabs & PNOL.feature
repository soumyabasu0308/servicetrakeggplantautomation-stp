﻿@STP
Feature: PNOL E2E routine visit with ERA
  
  @Sanity
  Scenario Outline: User should be successful to complete PNOL E2E routine visit with ERA
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>      
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails1> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I open the service point using manual entry <Barcode>
    Then I add new infestation with details as <ServiceCover1> <PestSpecies> <LocationDetails> <InfestationCount> <InfestationNotes>
    Then I add new treatment with details as <ServiceCover1> <Preparation> <LocationDetails> <Quantity> <Batch> <EPANumber> <ApplicationMethod> <EquipmentUsed> <AreaTreated> <TargetPests> <Treatment Notes>
    Then I add new recommendation with details as <RecommendationType> <RecommendationText> <Priority> <RecommendationDetailedText>
    Then on the record service screen I tap on the notes tab to add visit notes as <VisitNotesTextType> and <VisitNotesPreferredText>
    Then I navigate to work summary screen and validate <PestSpecies> <Preparation> <RecommendationDetailedText> <VisitNotes>
    Then I navigate to signature screen to capture customer signature <SignatureVisitDate>
    Then I end the visit 
    And  I validate the visit in the completed section in schedule screen with details as <CustomerName>
    And I close the mobile app <SUTDevice>     
    Given I open iCabs Application
    Then I search for <TechWorkGrid>
    And I entered the employee id, date of visit as <EmployeeId><DateOfVisit> to search
    And I validate the ERA details as <EmployeeId><DateOfVisit><ContractNumber><ERADetails> <Product>
    Then I open PNOL Application
    Then I search the site with site reference as <SiteReference> and <CustomerName>
    Then I validate the completed visit in PNOL with details as <ERADetails>
    Then I navigate to PNOL homepage and close the application

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails1 | SelectVisitType | ServiceCover1 | PestSpecies | LocationDetails | InfestationCount | InfestationNotes | Preparation | LocationDetails | Quantity | Batch | EPANumber | ApplicationMethod | EquipmentUsed | AreaTreated | TargetPests | Treatment Notes | RecommendationType | RecommendationText | Priority | RecommendationDetailedText | VisitNotesTextType | VisitNotesPreferredText | VisitNotes | TechWorkGrid | EmployeeId | DateOfVisit | VisitTypeICABS | Product | ERADetails | SignatureVisitDate | Barcode | SiteReference | Product |
      
      | com.kony.pestservicet |  SUT | Work View | 00820461 | Interserve | Customer | Pests - 12 | Routine | PESTS - 12 - R | Crickets  | Internal | Medium | Test infestation notes to add activity | Aquapy | External | 10 | 10 | 1232QW | Bait Station Updated | Cobra Light Trap | Area10 | Crickets | Test treatment notes to add treatment | Proofing | Door base gap | High | The base of the door | Routine | Internal Activity | Light mouse activity was noted | Tech Work Grid | 254218 | 28/01/2021 | R - Routine | PESTS | environmental risk assessment has been | 2021-01-28 | 1 | UK0082046100001 | PESTS |