﻿@STP
Feature: Update Infestation
  
  @Sanity
  Scenario Outline: User should be successful to update new infestation for pnol visit
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I open the service point using manual entry <Barcode>
    Then I add new infestation with details as <ServiceCover1> <PestSpecies> <LocationDetails> <InfestationCount> <InfestationNotes> 
    Then I update existing infestation <PestSpecies> with <UpdatedPestSpecies> and <InfestationCount>
    Then I discard the visit    

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails | VisitType | VisitDate | SelectVisitType | ServiceCover1 | PestSpecies | LocationDetails |InfestationCount | InfestationNotes | UpdatedPestSpecies | Barcode |
      
      | com.kony.pestservicet |  SUT | Work View | 00855807 | Chalegrove Properties Ltd | Customer | Pests - 12 | R | 28/09/2020 | Routine | Pests - 12 - R | Rats | Internal | Medium | Test infestation notes to add activity | Cat Trapping | TestServicePoint004 |