﻿@STP
Feature: End Day
  
  @Sanity
  Scenario Outline: User should be successful to end the day in STP
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I validate the active start day icon
    Then  I tap on enabled start day icon to validate the end day popup header<EndDayPopUpHeader>
    And I tap on end day button on the popup
    Then I validate the end day icon  
    And I close the mobile app <SUTDevice>     
    Given I open iCabs Application
    Then I search for <TechWorkGrid>
    And I entered the employee id, date of visit as <EmployeeId><DateOfVisit> to search
    And I validate the end day details in iCabs as <EmployeeId><DateOfVisit><Action>
    Then I close the iCabs application

    Examples:
      | ApplicationPackage | SUTDevice | Workview | EndDayPopUpHeader | TechWorkGrid | EmployeeId | DateOfVisit | Action |
      | com.kony.pestservicet |  SUT | Work View | Start / End Day | Tech Work Grid | 252201 | 25/06/2020 | Log Out |