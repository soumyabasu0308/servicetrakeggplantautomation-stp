﻿@STP
Feature: Delete visit notes
  
  @Sanity
  Scenario Outline: User should be successful to delete visit notes for any visit
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then on the record service screen I tap on the notes tab to add visit notes as <Barcode>, <VisitNotesTextType> and <VisitNotesPreferredText>
    Then on the notes tab I delete visit notes
    Then I discard the visit
    Then I close the mobile app <SUTDevice>
      
    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails | VisitType | VisitDate | SelectVisitType | VisitNotesTextType | VisitNotesPreferredText | Barcode |
      
      | com.kony.pestservicet |  SUT | Work View | 00820461 | Interserve | Customer | Pests - 12 | R | 21/12/2020 | Routine | Routine | Internal Activity | 2330858 |
      
      
      
