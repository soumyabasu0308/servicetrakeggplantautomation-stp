﻿@STP
Feature: Non-PNOL E2E routine visit with multiple service covers
  
  @Sanity
  Scenario Outline: User should be successful to complete Non-PNOL E2E routine visit with multiple service covers
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>      
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a non-pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails1> and <ServiceCoverDetails2> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I add new infestation with details as <ServiceCover1> <PestSpecies1> <LocationDetails1> <InfestationCount1> <InfestationNotes1> <ServiceCover2> <PestSpecies2> <LocationDetails2> <InfestationCount2> <InfestationNotes2>
    Then I add new treatment with details as <ServiceCover1> <Preparation1> <LocationDetails1> <Quantity1> <Batch1> <EPANumber1> <ApplicationMethod1> <EquipmentUsed1> <AreaTreated1> <TargetPests1> <TreatmentNotes1> <ServiceCover2> <Preparation2> <LocationDetails2> <Quantity2> <Batch2> <EPANumber2> <ApplicationMethod2> <EquipmentUsed2> <AreaTreated2> <TargetPests2> <TreatmentNotes2>
    Then I add new recommendation with details as <ServiceCover1> <RecommendationType1> <RecommendationText1> <Priority1> <RecommendationDetailedText1> <ServiceCover2> <RecommendationType2> <RecommendationText2> <Priority2> <RecommendationDetailedText2>
    Then on the record service screen I tap on the notes tab to add visit notes as <VisitNotesTextType> and <VisitNotesPreferredText>
    Then I navigate to work summary screen and validate <PestSpecies1> <Preparation1> <RecommendationDetailedText1> <PestSpecies2> <Preparation2> <RecommendationDetailedText2> <VisitNotes>
    Then I navigate to signature screen to capture customer signature <SignatureVisitDate>
    Then I end the visit 
    Then I validate the apportion time
    And  I validate the visit in the completed section in schedule screen with details as <CustomerName>
    And I close the mobile app <SUTDevice>     
    Given I open iCabs Application
    Then I search the completed visit in tech work grid
    And I entered the employee id, date of visit as <EmployeeId><DateOfVisit> to search
    Then I validate the completed visit details for first service cover in iCabs as <EmployeeId><DateOfVisit> <ContractNumber> <VisitTypeICABS> <PestSpecies1> <Preparation1> <RecommendationDetailedText1> <Product1>
    And I validate the completed visit details for second service cover in iCabs as <EmployeeId><DateOfVisit> <ContractNumber> <VisitTypeICABS> <PestSpecies2> <Preparation2> <RecommendationDetailedText2> <Product2>

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails1 | ServiceCoverDetails2 | SelectVisitType | ServiceCover1 | PestSpecies1 | LocationDetails1 | InfestationCount1 | InfestationNotes1 | ServiceCover2 | PestSpecies2 | LocationDetails2 | InfestationCount2 | InfestationNotes2 | Preparation1 | LocationDetails1 | Quantity1 | Batch1 | EPANumber1 | ApplicationMethod1 | EquipmentUsed1 | AreaTreated1 | TargetPests1 | TreatmentNotes1 | Preparation2 | LocationDetails2 | Quantity2 | Batch2 | EPANumber2 | ApplicationMethod2 | EquipmentUsed2 | AreaTreated2 | TargetPests2 | TreatmentNotes2 | RecommendationType1 | RecommendationText1 | Priority1 | RecommendationDetailedText1 | RecommendationType2 | RecommendationText2 | Priority2 | RecommendationDetailedText2| VisitNotesTextType | VisitNotesPreferredText | VisitNotes | EmployeeId | DateOfVisit | VisitTypeICABS | Product1 | Product2 | SignatureVisitDate |
      
      | com.kony.pestservicet |  SUT | Work View | 00285626 | E Klein | Customer | Pests - 8 | EFK Non Rentokil Maintenance Only - 4 | Routine | PESTS - 8 - R | Crickets  | Internal | Medium | Test infestation notes to add first activity | OTM - 4 - R | Earwigs  | Internal | Medium | Test infestation notes to add second activity | Aquapy | Internal | 10 | 10 | 1232QW | Bait Station Updated | Cobra Light Trap | Area10 | American Cockroaches | Test treatment notes to add treatment | Aquapy | Internal | 20 | 20 | 1232QW | Bait Station Updated | Cobra Light Trap | Area20 | Earwigs | Test treatment notes to add second treatment | Proofing | Door base gap | High | The base of the door | Proofing | Doorframe | High | The doorframe in this | Routine | Internal Activity | Light mouse activity was noted | 254218 | 10/02/2021 | R - Routine | PESTS | OTM | 2021-02-10 |