﻿@STP
Feature: E2E PNOL job visit
  
  @Sanity
  Scenario Outline: User should be successful to complete E2E PNOL job visit
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I open the service point using manual entry <Barcode>
    Then I add new infestation with details as <ServiceCover1> <PestSpecies> <LocationDetails> <InfestationCount> <InfestationNotes> 
    Then I add new treatment with details as <ServiceCover1> <Preparation> <LocationDetails> <Quantity> <Batch> <EPANumber> <ApplicationMethod> <EquipmentUsed> <AreaTreated> <TargetPests> <TreatmentNotes>
    Then I add new recommendation with details as <RecommendationType> <RecommendationText> <Priority> <RecommendationDetailedText>
    Then I add new rentokil task action with details as <RentokilActionType> <RentokilActionSubType> <RentokilActionDetailText>
    Then on the record service screen I tap on the notes tab to add visit notes as <VisitNotesTextType> and <VisitNotesPreferredText>
    Then I navigate to work summary screen and validate <PestSpecies> <Preparation> <RecommendationDetailedText> <RentokilActionDetailText> <VisitNotes>
    Then I navigate to signature screen to capture customer signature <SignatureVisitDate>
    Then I end the visit 
    And  I validate the visit in the completed section in schedule screen with details as <CustomerName>
    And I close the mobile app <SUTDevice> 
    Given I open iCabs Application
    Then I search for <TechWorkGrid>
    And I entered the employee id, date of visit as <EmployeeId><DateOfVisit> to search
    And I validate the completed visit details in iCabs as <EmployeeId><DateOfVisit> <ContractNumber> <VisitTypeICABS> <PestSpecies> <Preparation> <RecommendationDetailedText> <Product>
#    Then I open PNOL Application
#    Then I search the site with site reference as <SiteReference> and <CustomerName>
#    Then I validate the completed visit in PNOL with details as <DateOfVisit>
#    Then I navigate to PNOL homepage and close the application 

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails | VisitType | VisitDate | SelectVisitType | ServiceCover1 | PestSpecies | LocationDetails |InfestationCount | InfestationNotes | Barcode | Preparation | LocationDetails |Quantity | Batch | EPANumber | ApplicationMethod | EquipmentUsed | AreaTreated | TargetPests | Treatment Notes | RecommendationType | RecommendationText | Priority | RecommendationDetailedText | RentokilActionType | RentokilActionSubType | RentokilActionDetailText | VisitNotesTextType | VisitNotesPreferredText | VisitNotes | TechWorkGrid | EmployeeId | DateOfVisit | VisitTypeICABS | Product | SiteReference | SignatureVisitDate |
      
      | com.kony.pestservicet |  SUT | Work View | 00873593 | Test Pnol Job | Customer | Pests - 4 | R | 05/01/2021 | Routine | PESTS - 4 - R | Rats | Internal | Medium | Test infestation notes to add activity | 1981838 | Aquapy | Internal | 10 | 10 | 1232QW | Bait Station Updated | Cobra Light Trap | Area10 | American Cockroaches | Test treatment notes to add treatment | Proofing | Door base gap | High | The base of the door | Radar | Radar Battery | A new battery has been installed | Routine | Internal Activity |  Light mouse activity was noted | Tech Work Grid | 254218 | 18/01/2021 | R - Routine | PESTS | UK0087359300001 | 2021-01-18 |