﻿@STP
Feature: Non Pnol Visit - Validation of end with issue and check the same in iCabs
  
  @Sanity
  Scenario Outline: User should be successful to complete Non-PNOL E2E routine visit with end with issue
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>      
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a non-pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails1> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I from the menu I end the visit with issue type <IssueType>            
    And  I validate the visit in the completed section in schedule screen with details as <CustomerName>
    And I close the mobile app <SUTDevice> 
    Then I validate the visit in iCabs using API <ContractNumber> <PremiseNumber> <IssueCode> <IssueTypeCode> <VisitDate>        

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | PremiseNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails1 | SelectVisitType | IssueType |  IssueCode | IssueTypeCode | VisitDate |
      
      | com.kony.pestservicet |  SUT | Work View | 00815163 | 1 | Abu Tajwer | Customer | Pests - 6 | Routine | Customer Unavailable |  CU | W | 2021-02-10 |