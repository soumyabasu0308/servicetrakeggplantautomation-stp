﻿@STP
Feature: Login STP
  
  @Sanity
  Scenario Outline: User should be successful to login into STP
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on the <SelectAccount> pop-up 
    Then  I select an email <EmailId>
    And I tap on <AcceptEmail> to validate <SelectCountryBusinessPopUp>
    Then I select the country and business code as <Source>
    And I tap on confirm button <SubmitButton> and validate technician name <TechName>
    Then I validate I am on schedule screen <Workview>
      
    Examples:
      | ApplicationPackage | SUTDevice | SelectAccount | EmailId | AcceptEmail | SelectCountryBusinessPopUp | Source | SubmitButton | TechName | Workview |
      | com.kony.pestservicet |  SUT | Choose an account | service.trak3 | OK | Settings | UK D | Confirm | Michael Freeman | Work View |