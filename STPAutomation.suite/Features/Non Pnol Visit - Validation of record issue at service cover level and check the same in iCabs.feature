﻿@STP
Feature: Non-PNOL E2E record issue at service cover level
  
  @Sanity
  Scenario Outline: User should be successful to complete Non-PNOL E2E routine visit with record issue at service cover level
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>      
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a non-pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails1> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button      
    Then on the record service screen I tap on the notes tab record issue as <IssueType>
    Then I navigate to work summary screen and validate <IssueType>
    Then I navigate to signature screen to capture add the no signature reason as <NoSignatureReason>
    Then I end the visit 
    And  I validate the visit in the completed section in schedule screen with details as <CustomerName>
    And I close the mobile app <SUTDevice> 
    Then I validate the visit in iCabs using API <ContractNumber> <PremiseNumber> <IssueCode> <IssueTypeCode> <VisitDate>        

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | PremiseNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails1 | SelectVisitType | IssueType | NoSignatureReason | IssueCode | IssueTypeCode | VisitDate |
      
      | com.kony.pestservicet |  SUT | Work View | 00386514 | 49 | Goldsmiths | Customer | Pests - 8 | Routine | Premises Closed | Customer Unavailable | PC | O | 2021-02-11 |