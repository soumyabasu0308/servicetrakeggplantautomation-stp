﻿@STP
Feature: Update rentokil action or task
  
  @Sanity
  Scenario Outline: User should be successful to update new rentokil action or task for pnol visit
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I open the service point using manual entry <Barcode>
    Then I add new rentokil task action with details as <RentokilActionType><RentokilActionSubType><RentokilActionDetailText> and update the same with details as <VisitDate><RentokilActionTypeUpdated><RentokilActionSubTypeUpdated><RentokilActionDetailTextUpdated>
    Then I discard the visit    

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails | VisitType | VisitDate | SelectVisitType | ServiceCover1 | RentokilActionType | RentokilActionSubType | RentokilActionDetailText | Barcode | RentokilActionTypeUpdated |RentokilActionSubTypeUpdated | RentokilActionDetailTextUpdated |
      
      | com.kony.pestservicet |  SUT | Work View | 00415278 | 747 Zizzi Canary Wharf | Customer | Pests - 12 | R | 15/02/2021 | Routine | Pests - 12 - R | Radar | Radar Battery | A new battery has been installed | 13240245 | Other | Damaged Mothpot | Damaged Mothpot |