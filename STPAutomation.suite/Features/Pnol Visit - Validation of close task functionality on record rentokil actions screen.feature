﻿@STP
Feature: Close rentokil tasks or action
  
  @Sanity
  Scenario Outline: User should be successful to close rentokil task or action for pnol visit
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I open the service point using manual entry <Barcode>
    Then I add new rentokil task action with details as <RentokilActionType> <RentokilActionSubType> <RentokilActionDetailText>
    Then I tap on the notes tab to add visit notes as <VisitNotesTextType> and <VisitNotesPreferredText>
    Then I navigate to signature screen to capture customer signature <SignatureVisitDate>
    Then I end the visit 
    Then I wait for visit to be synced successfully
    Then I open PNOL Application
    Then I search the site with site reference as <SiteReference> and <CustomerName>
    Then I validate the rentokil tasks or actions in PNOL as <RentokilActionDetailText>
    Then I navigate to PNOL homepage and close the application
    Then I launch the application <ApplicationPackage> in the connected device <SUTDevice>  
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I open the service point using manual entry <Barcode>
    Then I close existing rentokil actions with details as <VisitDate> and <RentokilActionDetailText>
    Then I tap on the notes tab to add visit notes as <VisitNotesTextType> and <VisitNotesPreferredText>
    Then I navigate to signature screen to capture customer signature <SignatureVisitDate>
    Then I end the visit
    Then I wait for visit to be synced successfully
    Then I open PNOL Application
    Then I search the site with site reference as <SiteReference> and <CustomerName>
    Then I validate the closed rentokil actions in PNOL as <VisitDate> and <RecommendationDetailedText>
    Then I navigate to PNOL homepage and close the application

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | SiteReference | CustomerName | CustomerScreenHeader | ServiceCoverDetails | VisitType | VisitDate | SelectVisitType | ServiceCover1 | RentokilActionType | RentokilActionSubType | RentokilActionDetailText | Barcode | VisitNotesTextType | VisitNotesPreferredText | SignatureVisitDate |
      
      | com.kony.pestservicet |  SUT | Work View | 00415278 | UK0041527800747 | 747 Zizzi Canary Wharf | Customer | Pests - 12 | R | 02/03/2021 | Routine | Pests - 12 - R | Radar | Radar Battery | A new battery has been installed | 13240245 | Routine | Internal Activity | 2021-03-02 |