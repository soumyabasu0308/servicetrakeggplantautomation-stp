﻿@STP
Feature: Update Service Point
  
  @Sanity
  Scenario Outline: User should be successful to update service point for pnol visit
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I add a new service point on the location screen with details <Barcode><Building><Floor><Department><DetectorType><Detector><ServicePointDescription>      
    Then I discard the visit
    Then I close the mobile app <SUTDevice> 
    Then I open PNOL Application
    Then I search the site with site reference as <SiteReference> and <CustomerName>
    Then I validate the service point in PNOL with details as <Building> <Floor> <Department><ServicePointDescription>
    Then I navigate to PNOL homepage and close the application
    Then I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I update a new service point on the location screen with details <Barcode><Building><Floor><Department><ServicePointDescription><UpdatedServicePointDescription>
    Then I discard the visit
    Then I close the mobile app <SUTDevice>
    Then I open PNOL Application
    Then I search the site with site reference as <SiteReference> and <CustomerName>
    Then I validate the service point in PNOL with details as <Building> <Floor> <Department><UpdatedServicePointDescription>
    Then I navigate to PNOL homepage and close the application
    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails | VisitType | VisitDate | SelectVisitType | ServiceCover1 | PestSpecies | LocationDetails |InfestationCount | InfestationNotes | Barcode | Building | Floor | Department | DetectorType | Detector | ServicePointDescription | SiteReference | UpdatedServicePointDescription |
      
      | com.kony.pestservicet |  SUT | Work View | 00834422 | Canary Wharf | Customer | Pests - 7 | R | 28/09/2020 | Routine | Pests - 7 - R | Rats | Internal | Medium | Test infestation notes to add activity | 111111 | TESTB1 | TESTF1 | TESTD1 | Rodent Toxic | Internal rat box | TESTSP1 | UK0083442200014| UTESTSP1 |