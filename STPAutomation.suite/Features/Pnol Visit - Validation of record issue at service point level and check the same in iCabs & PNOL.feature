﻿@STP
Feature: PNOL E2E record issue at service point level
  
  @Sanity
  Scenario Outline: User should be successful to complete PNOL E2E routine visit with record issue at point cover level
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>      
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a non-pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails1> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I open a service point to record issue <ServicePointDetails>     
    Then on the service point details screen I tap on the issue icon to record issue
    Then I navigate to work summary screen and validate <IssueType>
    Then I navigate to signature screen to capture add the no signature reason as <NoSignatureReason>
    Then I end the visit 
    And  I validate the visit in the completed section in schedule screen with details as <CustomerName>
    And I close the mobile app <SUTDevice> 
    Then I open PNOL Application
    Then I search the site with site reference as <SiteReference> and <CustomerName>
    Then I validate the completed visit in PNOL with details as <EmployeeId>
    Then I navigate to PNOL homepage and close the application         

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | PremiseNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails1 | SelectVisitType | IssueType | NoSignatureReason | EmployeeId | SiteReference | VisitDate | ServicePointDetails |
      
      | com.kony.pestservicet |  SUT | Work View | 00709717 | 1 | Billingsgate | Customer | Pests - 8 | Routine | No Keys | Customer Unavailable | 254218 | UK0070971700001 | 2021-01-19 | TSP1 |