﻿@STP
Feature: Delete recommendation comment
  
  @Sanity
  Scenario Outline: User should be successful to delete recommendation comment for pnol visit
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I open the service point using manual entry <Barcode>
    Then I add new recommendation with details as <RecommendationType> <RecommendationText> <Priority> <RecommendationDetailedText>
    Then I tap on the notes tab to add visit notes as <VisitNotesTextType> and <VisitNotesPreferredText>
    Then I navigate to signature screen to capture customer signature <SignatureVisitDate>
    Then I end the visit 
    Then I wait for visit to be synced successfully
    When I am on schedule screen <Workview>
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails>, visit type <VisitType> and visit date <VisitDate> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I open the service point using manual entry <Barcode>
    Then I close existing recommendation <RecommendationDetailedText>
    Then I tap on the notes tab to add visit notes as <VisitNotesTextType> and <VisitNotesPreferredText>
    Then I navigate to signature screen to capture customer signature <SignatureVisitDate>
    Then I end the visit
    Then I close the mobile app <SUTDevice>
    Then I open PNOL Application
    Then I search the site with site reference as <SiteReference> and <CustomerName>
    Then I validate the recommendation in PNOL as <VisitDate> and <RecommendationDetailedText>
    Then I navigate to PNOL homepage and close the application

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | SiteReference | CustomerName | CustomerScreenHeader | ServiceCoverDetails | VisitType | VisitDate | SelectVisitType | ServiceCover1 | RecommendationType | RecommendationText | Priority | RecommendationDetailedText | Barcode | VisitNotesTextType | VisitNotesPreferredText | SignatureVisitDate |
      
      | com.kony.pestservicet |  SUT | Work View | 00415275 | UK0041527502451 | 2451 Pizza Express | Customer | Pests - 26 | R | 10/11/2020 | Routine | Pests - 26 - R | Proofing | Door base gap | High | The base of the door | 13240234 | Routine | Internal Activity | 2021-02-15 |