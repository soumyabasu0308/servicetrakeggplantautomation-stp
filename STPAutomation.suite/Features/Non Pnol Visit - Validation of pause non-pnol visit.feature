﻿@STP
Feature: Non-PNOL paused visit
  
  @Sanity
  Scenario Outline: User should be successful to pause visit
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>      
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a non-pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails1> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I validate that I can pause the visit
      
    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails1 | SelectVisitType |
      
      | com.kony.pestservicet |  SUT | Work View | 00863098 | TD Tom Davies Limited | Customer | PAYGL 4 RMID + Chargeable Callouts - 4 | Routine |