﻿@STP
Feature: Non-PNOL E2E follow up visit with single service cover
  
  @Sanity
  Scenario Outline: User should be successful to complete Non-PNOL E2E follow up visit with single service cover
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>      
    Then I navigate to portfolio screen
    And  I tap on search icon
    And I search for a non-pnol contract <ContractNumber> and validate the customer name <CustomerName>
    Then I tap on the searched customer name <CustomerName> and validate I am on the customer details screen <CustomerScreenHeader>
    And I tap on the start visit icon and validate I am on the start visit popup
    And I validate the service cover details <ServiceCoverDetails1> on the start visit pop
    Then I select the required visit type <SelectVisitType> and tap the start visit button
    Then I add new infestation with details as <ServiceCover1> <PestSpecies> <LocationDetails> <InfestationCount> <InfestationNotes>
    Then I add new treatment with details as <ServiceCover1> <Preparation> <LocationDetails> <Quantity> <Batch> <EPANumber> <ApplicationMethod> <EquipmentUsed> <AreaTreated> <TargetPests> <Treatment Notes>
    Then I add new recommendation with details as <RecommendationType> <RecommendationText> <Priority> <RecommendationDetailedText>
    Then on the record service screen I tap on the notes tab to add visit notes as <VisitNotesTextType> and <VisitNotesPreferredText>
    Then I navigate to work summary screen and validate <PestSpecies> <Preparation> <RecommendationDetailedText> <VisitNotes>
    Then I navigate to signature screen to capture customer signature <SignatureVisitDate>
    Then I end the visit 
    And  I validate the visit in the completed section in schedule screen with details as <CustomerName>
    And I close the mobile app <SUTDevice>     
    Given I open iCabs Application
    Then I search for <TechWorkGrid>
    And I entered the employee id, date of visit as <EmployeeId><DateOfVisit> to search
    And I validate the completed visit details in iCabs as <EmployeeId><DateOfVisit> <ContractNumber> <VisitTypeICABS> <PestSpecies> <Preparation> <RecommendationDetailedText> <Product>

    Examples:
      | ApplicationPackage | SUTDevice | Workview | ContractNumber | CustomerName | CustomerScreenHeader | ServiceCoverDetails1 | SelectVisitType | ServiceCover1 | PestSpecies | LocationDetails | InfestationCount | InfestationNotes | Preparation | LocationDetails | Quantity | Batch | EPANumber | ApplicationMethod | EquipmentUsed | AreaTreated | TargetPests | Treatment Notes | RecommendationType | RecommendationText | Priority | RecommendationDetailedText | VisitNotesTextType | VisitNotesPreferredText | VisitNotes | TechWorkGrid | EmployeeId | DateOfVisit | VisitTypeICABS | Product | SignatureVisitDate |
      
      | com.kony.pestservicet |  SUT | Work View | 37003378 | Thomas Pink | Customer | Pests - 8 | Follow | PESTS - 8 - F | Crickets  | Internal | Medium | Test infestation notes to add activity | Aquapy | Internal | 10 | 10 | 1232QW | Bait Station Updated | Cobra Light Trap | Area10 | American Cockroaches | Test treatment notes to add treatment | Proofing | Door base gap | High | The base of the door | Follow Up | Rodent Activity | A follow up visit was carried out | Tech Work Grid | 254218 | 10/02/2021 | F - Follow-up  | PESTS | 2021-02-10 |