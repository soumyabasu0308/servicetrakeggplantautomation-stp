﻿@STP
Feature: Start Day
  
  @Sanity
  Scenario Outline: User should be successful to start the day in STP
    Given I launch the application <ApplicationPackage> in the connected device <SUTDevice>
    When I am on schedule screen <Workview>
    Then  I tap on disabled start day icon to validate the start day popup header<StartDayPopUpHeader>
    And I tap on <StartDay> button on the popup
    Then I validate the active start day icon
    And I close the mobile app <SUTDevice>     
    Given I open iCabs Application
    Then I search for <TechWorkGrid>
    And I entered the employee id, date of visit as <EmployeeId><DateOfVisit> to search
    And I validate the start day details in iCabs as <EmployeeId><DateOfVisit><Action>

    Examples:
      | ApplicationPackage | SUTDevice | Workview | StartDayPopUpHeader | StartDay | TechWorkGrid | EmployeeId | DateOfVisit | Action |
      | com.kony.pestservicet |  SUT | Work View | Start / End Day | Start Day | Tech Work Grid | 254218 | 01/02/2021 | Log In |